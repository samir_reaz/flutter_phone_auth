import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phone_auth/home_page.dart';
import 'package:flutter_phone_auth/login_page.dart';

class AuthService {
  // Handles Auth
  handleAuth() {
    return StreamBuilder(
      stream: FirebaseAuth.instance.onAuthStateChanged,
      builder: (BuildContext context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: Text('Loading'));
        } else {
          if (snapshot.hasData)
            return HomePage();
          else
            return LoginPage();
        }
      },
    );
  }

  //Sign out
  signOut() {
    FirebaseAuth.instance.signOut();
  }

  // Sign IN
  signIn(AuthCredential authCredential) {
    FirebaseAuth.instance.signInWithCredential(authCredential);
  }

  signInWithOTP(smsCode, verId) {
    AuthCredential authCredential = PhoneAuthProvider.getCredential(
      verificationId: verId,
      smsCode: smsCode,
    );
    signIn(authCredential);
  }
}
